<?php
  class Docente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("docente",
                $datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    function obtenerTodos(){
      $listadoDocentes=$this->db->get("docente");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoDocentes->num_rows()>0) { //
        return $listadoDocentes->result();
      }else{
        return false;
      }

    }
    function borrar($id_doc)
    {
      //delete from instructor where id_ins = 1:
      $this->db->where("id_doc",$id_doc);
      //return $this->db->delete("instructor");    <----- OCPION 2 para eliminar
      if ($this->db->delete("docente")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
