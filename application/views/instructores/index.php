<h1> <b>LISTADO DE INSTRUCTORES</b></h1>
<br>
<?php if ($instructores): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:blue ; color:white">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER APELLIDO</th>
             <th>SEGUNDO APELLIDO</th>
             <th>NOMBRES</th>
             <th>TITULO</th>
             <th>TELEFONO</th>
             <th>DIRECCIÓN</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:gray ;color:black">
           <?php foreach ($instructores as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_ins;?></td>
               <td><?php echo $filaTemporal->cedula_ins; ?></td>
               <td><?php echo $filaTemporal->primer_apellido_ins; ?></td>
               <td><?php echo $filaTemporal->segundo_apellido_ins; ?></td>
               <td><?php echo $filaTemporal->nombres_ins; ?></td>
               <td><?php echo $filaTemporal->titulo_ins; ?></td>
               <td><?php echo $filaTemporal->telefono_ins; ?></td>
               <td><?php echo $filaTemporal->direccion_ins; ?></td>
               <td class="text-center">
                 <a href="#" title="Editar">
                   <i class="glyphicon glyphicon-pencil" style="color:orange"></i>
                 </a>
                 &nbsp; &nbsp; &nbsp;
                 <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->id_ins;?>" title="Eliminar">
                   <i class="glyphicon glyphicon-trash" style="color:red"></i>
                 </a>
               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No hay instructores</h1>
       <?php endif; ?>
  </table>



<!-- <?php if ($instructores): ?>
  <?php print_r($instructores); ?>
<?php else: ?>
  <h2>NO EXISTEN DATOS</h2>
<?php endif; ?> -->
