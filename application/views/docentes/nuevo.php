<h1>NUEVO DOCENTE</h1>
<form class=""
action="<?php echo site_url(); ?>/docentes/guardar"
method="post">
<!-- //en action llamamos al site_url al controlador DOCENTESy funcion GURADAR -->
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_doc" value=""
          id="cedula_doc">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          name="apellido_doc" value=""
          id="apellido_doc">
      </div>

    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          name="nombre_doc" value=""
          id="nombre_doc">
      </div>
      <div class="col-md-4">
          <label for="">Título:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          name="titulo_doc" value=""
          id="titulo_doc">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_doc" value=""
        id="telefono_doc">
      </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/docentes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
