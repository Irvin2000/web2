<h1> <b>LISTADO DE DOCENTES</b></h1>
<br>
<?php if ($docentes): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:blue ; color:white">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>APELLIDO</th>
             <th>NOMBRES</th>
             <th>TITULO</th>
             <th>TELEFONO</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:gray ;color:black">
           <?php foreach ($docentes as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_doc;?></td>
               <td><?php echo $filaTemporal->cedula_doc; ?></td>
               <td><?php echo $filaTemporal->apellido_doc; ?></td>
               <td><?php echo $filaTemporal->nombre_doc; ?></td>
               <td><?php echo $filaTemporal->titulo_doc; ?></td>
               <td><?php echo $filaTemporal->telefono_doc; ?></td>

               <td class="text-center">
                 <a href="#" title="Editar">
                   <i class="glyphicon glyphicon-pencil" style="color:orange"></i>
                 </a>
                 &nbsp; &nbsp; &nbsp;
                 <a href="<?php echo site_url(); ?>/docentes/eliminar/<?php echo $filaTemporal->id_doc;?>" title="Eliminar">
                   <i class="glyphicon glyphicon-trash" style="color:red"></i>
                 </a>
               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No hay Docentes</h1>
       <?php endif; ?>
  </table>



<!-- <?php if ($instructores): ?>
  <?php print_r($instructores); ?>
<?php else: ?>
  <h2>NO EXISTEN DATOS</h2>
<?php endif; ?> -->
