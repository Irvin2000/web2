<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docentes extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model("docente");
	}

	public function index()
	{
		$data['docentes']=$this->docente->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('docentes/index',$data);
		$this->load->view('footer')	;
	}
  public function nuevo()
  {
    $this->load->view('header')	;
    $this->load->view('docentes/nuevo');
    $this->load->view('footer')	;
  }
	public function guardar()
	{
		$datosNuevoDocente=array(
			"cedula_doc"=>$this->input->post('cedula_doc'),
			"apellido_doc"=>$this->input->post('apellido_doc'),
			"nombre_doc"=>$this->input->post('nombre_doc'),
			"titulo_doc"=>$this->input->post('titulo_doc'),
			"telefono_doc"=>$this->input->post('telefono_doc')
		);
	if ($this->docente->insertar($datosNuevoDocente)) {
		redirect('docentes/index');

	}else{
		echo "<h1>ERROR AL INSERTAR</h1>";

	}
		// p<rint_r($datosNuevoInstructor);
	}
	public function eliminar($id_doc)
	{
		if ($this->docente->borrar($id_doc)) {
			redirect ('docentes/index');
		} else {
			echo "ERROR AL ELIMINAR";
		}

	}





} // cierre de la clase
